//Récupère les données avec des posts en appelant "recup.php"
function recupdata()
{
	$.post('recup.php',function(data){
		//Appelle la classe "affichage" ce trouvant dans index.php pour afficher
		$('.affichage').html(data);
	});
}

//Permet de lancer la fonction "recupdata" et faire une pause de une seconde.
$(document).ready(function(){
	recupdata();

	setInterval(recupdata, 1000);
});