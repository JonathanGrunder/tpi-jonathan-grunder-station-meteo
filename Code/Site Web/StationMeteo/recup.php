
<?php
//Connection à la base de donnée
$db= new PDO("mysql:host=localhost;dbname=stationmeteo", 'root','');

//Requete pour récupérer toutes les données de la table "centrale" ou l'id est de "1"
$req = "SELECT * FROM centrale WHERE idEntreeCentrale=1";

$query = $db->prepare($req);
$query->execute();

while ($ligne = $query->fetch()) {
	$centraleTemp = $ligne["temperature"];
	$centraleHumi = $ligne["humidity"];
	$centraleGas = $ligne["gas"];
	?>
	 <table>
	 	<thead>
	 	<tr>
	 		<td colspan="2">
	 			<h1>
	 				centrale
	 			</h1>
	 		</td>
	 	</tr>
	 	</thead>
	 	<tbody>
	 	<tr>
	 		<td class="temp">
	 			Temperature
	 		</td>
	 		<td>
	 			<?=$centraleTemp?>C°
	 		</td>
	 	</tr>
	 	<tr>
	 		<td>
	 			Humidité
	 		</td>
	 		<td>
	 			<?=$centraleHumi?>%
	 		</td>
	 	</tr>
	 	<tr>
	 		<td>
	 			Teneur en gaz
	 		</td>
	 		<td>
	 			<?=$centraleGas?> ppm
	 		</td>
	 		<br>
	 	</tr>
	 	</tbody> 	 
	 </table>
<?php } 

//Requete pour récupérer toutes les données de la table "station" ou l'id est de "1"
$req = "SELECT * FROM station WHERE idEntreeStation=1";

$query = $db->prepare($req);
$query->execute();

while ($ligne = $query->fetch()) 
{
	$stationTemp = $ligne["temperature"];
	$stationHumi = $ligne["humidity"];
	$stationGas = $ligne["gas"];
	$stationLux = $ligne["Lux"];
	$stationUV = $ligne["UV"];
	$stationPress = $ligne["atmosphericpressure"];
	$stationAlti = $ligne["altitude"];

	?>
	 <table>
	 	<thead>
	 	<tr>
	 		<td colspan="2">
	 			<h1>
	 				Station
	 			</h1>
	 		</td>
	 	</tr>
	 	</thead>
	 	<tr>
	 		<td>
	 			Temperature
	 		</td>
	 		<td>
	 			<?=$stationTemp?>C°
	 		</td>
	 	</tr>
	 	<tr>
	 		<td>
	 			Humidité
	 		</td>
	 		<td>
	 			<?=$stationHumi?>%
	 		</td>
	 	</tr>
	 	<tr>
	 		<td>
	 			Teneur en gaz
	 		</td>
	 		<td>
	 			<?=$stationGas?> ppm
	 		</td>
	 		<br>
	 	</tr>
	 	<tr>
	 		<td>
	 			Luminosité
	 		</td>
	 		<td>
	 			<?=$stationLux?> lux
	 		</td>
	 		<br>
	 	</tr>
	 	<tr>
	 		<td>
	 			UV
	 		</td>
	 		<td>
	 			<?=$stationUV?> nm
	 		</td>
	 		<br>
	 	</tr>
	 	<tr>
	 		<td>
	 			Préssion athmosphérique
	 		</td>
	 		<td>
	 			<?=$stationPress?> pascale
	 		</td>
	 		<br>
	 	</tr>
	 	<tr>
	 		<td>
	 			Altitude
	 		</td>
	 		<td>
	 			<?=$stationAlti?> m
	 		</td>
	 		<br>
	 	</tr>
	 </table>
<?php
} 

if ($centraleTemp > $stationTemp) 
{
	$diffTemp = $centraleTemp - $stationTemp;
}else
{
	$diffTemp = $stationTemp - $centraleTemp;
}

if ($centraleHumi > $stationHumi) 
{
	$diffHumi = $centraleHumi - $stationHumi;
}else
{
	$diffHumi = $stationHumi - $centraleHumi;
}

if ($centraleGas > $stationGas) 
{
	$diffGas = $centraleGas - $stationGas;
}else
{
	$diffGas = $stationGas - $centraleGas;
}

?>
	 	 <table>
	 	<thead>
	 	<tr>
	 		<td colspan="2">
	 			<h1>
	 				Différence entre Centrale & Station
	 			</h1>
	 		</td>
	 	</tr>
	 	</thead>
	 	<tr>
	 		<td>
	 			Temperature
	 		</td>
	 		<td>
	 			<?=$diffTemp?>C°
	 		</td>
	 	</tr>
	 	<tr>
	 		<td>
	 			Humidité
	 		</td>
	 		<td>
	 			<?=$diffHumi?>%
	 		</td>
	 	</tr>
	 	<tr>
	 		<td>
	 			Teneur en gaz
	 		</td>
	 		<td>
	 			<?=$diffGas?> ppm
	 		</td>
	 		<br>
	 	</tr> 
	 </table>
<?php ?>