/*
  Auteur : Jonathan Grunder
  Version : 1.0
  Date de création : 23.05.2019
  Date de dernière modification : 24.05.2019

  Ce code a pour but de mettre à jour une base de donnée qui sert à afficher les données récupéré par l'arduino

  La requete update est utilisé pour ne pas remplire inutilement la BDD. Vu que son unique but est d'afficher les dernières valeurs récupéré.
  Ceci simplifie aussi le code coté WEB

  Il faut déjà avoir une entrée dans la BDD
*/

#include <mysql.h>
#include <Wire.h>

#include <HP20x_dev.h>
#include <KalmanFilter.h>

#include <DHT.h>
#define DHTPIN 2

#define DHTTYPE DHT11

DHT dht(DHTPIN, DHTTYPE);

KalmanFilter p_filter;    //filtre de pression
char *host, *user, *pass, *db; //Initialisation des variables pour la connexion à la BDD
char tBuffer[150]; //Buffer pour les requetes
void setup()
{
  Serial.begin(9600);
  /*
     On ajoute les valeurs aux variables pour se connecter à la BDD
  */
  host = "localhost";
  user = "root";
  pass = "";
  db = "stationmeteo";
  //Connexion a la base de donnée, retourne true or false. On affiche si oui ou non la connexion c'est bien passé à l'utilisateur
  if (mysql_connect(host, user, pass, db)) {

    Serial.print("Connected to ");
    Serial.println(host);

  } else {
    Serial.println("Connection failed.");
  }
  dht.begin(); //Debut le capteur de temperature et humidité
  HP20x.begin(); //Débute le capteur de pression atmosphérique
}

void loop()
{
  /*
   * Appelle des fonctions pour récupérer les données des capteurs
   */
  int gas = getGas();
  float atmopress = getPressure();
  int alti = getAltitude();

  /*
     Initisalisation des valeurs pour les ajouter à la requète.
     dans l'ordre
     p = pression atmosphérique
     a = altitude
     l = lux
     u = UV
     g = gas
     t = température
     h = humidité
     le float ne peut pas être utilisé sinon la construction de la requêtes ne fonctionne pas
  */
  int p = atmopress;
  int a = alti;
  int l = analogRead(A1);
  int u = analogRead(A2);;
  int g = gas;
  int t = dht.readTemperature();
  int h = dht.readHumidity();
  delay(300); //La récupération du capteur de temperature et humidité prends environ 250 milliseconde.
  
    //On met la requete dans un buffer
    sprintf(tBuffer, "UPDATE station SET temperature = %d, humidity = %d, gas = %d, Lux = %d, UV = %d, atmosphericpressure = %d, altitude = %d   WHERE idEntreeStation = 1", t, h, g, l, u, p, a); //utilisation de l'update pour ne pas avoir à
    //Execution de la requete
    int result = mysql_query(tBuffer);
    if (result) {
      Serial.print("requete effectuee");
    } else {
      Serial.println("Requete non effectuée");
    }

  delay(3000); //delais de 3 seconde
}

/*
   Fonction permetant de récuperer la teneur en gaz dans l'air.
*/
int getGas()
{
  float sensor_volt;
  float RS_gas;
  float ratio;
  float R0 = 0.91; //cette valeur dépends du calibrage effectué avant
  int sensorValue = analogRead(A0); //lecture du port A0 ou ce trouve le capteur de gaz
  delay(300); //Délais pour être sur que la lecture du port A0 à était faite avant de procèder au calcule
  /*-------------------------------------------------
     Calcule pour pouvoir sortir la teneur des gaz (LPG, CO, CH4)
  */
  sensor_volt = ((float)sensorValue / 1024) * 5.0;
  RS_gas = (5.0 - sensor_volt) / sensor_volt;
  ratio = RS_gas / R0; // ratio = RS/R0
  return ratio;
}

int getPressure()
{
  long Pressure = HP20x.ReadPressure();
  float p = Pressure/100.0;
  return p;
}

int getAltitude()
{
  int Altitude = HP20x.ReadAltitude();
  a = Altitude/100.0;
  return a;
}
