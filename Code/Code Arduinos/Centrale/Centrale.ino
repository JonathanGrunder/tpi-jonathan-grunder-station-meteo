/*
  Auteur : Jonathan Grunder
  Version : 1.0
  Date de création : 23.05.2019
  Date de dernière modification : 24.05.2019

  Ce code a pour but de mettre à jour une base de donnée qui sert à afficher les données récupéré par l'arduino

  La requete update est utilisé pour ne pas remplire inutilement la BDD. Vu que son unique but est d'afficher les dernières valeurs récupéré.
  Ceci simplifie aussi le code coté WEB

  Il faut déjà avoir une entrée dans la BDD
*/

#include <mysql.h>
#include <Wire.h>

#include "DHT.h"
#define DHTPIN 2

#define DHTTYPE DHT11

DHT dht(DHTPIN, DHTTYPE);


char *host, *user, *pass, *db; //Initialisation des variables pour la connexion à la BDD
char tBuffer[150]; //Buffer pour les requetes
void setup()
{
  Serial.begin(9600);
  /*
     On ajoute les valeurs aux variables pour se connecter à la BDD
  */
  host = "localhost";
  user = "root";
  pass = "";
  db = "stationmeteo";
  //Connexion a la base de donnée, retourne true or false. On affiche si oui ou non la connexion c'est bien passé à l'utilisateur
  if (mysql_connect(host, user, pass, db)) {

    Serial.print("Connected to ");
    Serial.println(host);

  } else {
    Serial.println("Connection failed.");
  }
  dht.begin(); //Debut le capteur de temperature et humidité
}

void loop()
{
  /*
   * Appelle de la fonction getGas pour récuprérer la teneur en gas dans l'air en ppm
   */
  int gas = getGas();


  //Initisaliation de la variable de gaz et ajout de la valeur
  int g = gas; 
  int t = dht.readTemperature(); //Initialisation de la variable de temperature et lecture du capteur
  int h = dht.readHumidity(); //Initilisation de la variable d'humidité et lecture du capteur
  delay(300); //La récupération du capteur de temperature et humidité prends environ 250 milliseconde.


    //On met la requete dans un buffer
    sprintf(tBuffer, "UPDATE centrale SET temperature = %d, humidity = %d, gas = %d WHERE idEntreeCentrale = 1", t, h, g); //utilisation de l'update pour ne pas avoir à
    //Execution de la requete
    int result = mysql_query(tBuffer);
    //affiche sur le logiciel si la requete a bien été effectuée
    if (result) {
      Serial.println("requete effectuee");
    } else {
      Serial.println("Requete non effectuee");
    }


  delay(3000); //delais de 3 seconde
}

int getGas()
{
  float sensor_volt;
  float RS_gas;
  float ratio;
  float R0 = 0.91; //cette valeur dépends du calibrage effectué avant
  int sensorValue = analogRead(A0); //lecture du port A0 ou ce trouve le capteur de gaz
  delay(300); //Délais pour être sur que la lecture du port A0 à était faite avant de procèder au calcule
  /*-------------------------------------------------
     Calcule pour pouvoir sortir la teneur des gaz (LPG, CO, CH4)
  */
  sensor_volt = ((float)sensorValue / 1024) * 5.0;
  RS_gas = (5.0 - sensor_volt) / sensor_volt;
  ratio = RS_gas / R0; // ratio = RS/R0
  return ratio;
}
